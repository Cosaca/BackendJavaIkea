# BITBOX IKEA TEST BACKEND

## Dependencies

Run `mvn clean install` to download the dependencies the project needs. 

If you are using eclipse or IntelliJ IDEA you can use the tools that the IDE provides to install the dependencies.



## Development server

Run the spring application. It will serve the API at `localhost:8080`


