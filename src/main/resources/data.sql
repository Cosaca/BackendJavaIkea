INSERT INTO USER (username, surname, password, dni) VALUES
  ('Samuel', 'Valcarcel', '$2a$10$I9Xlsj8CUS9v6cZ2e9FkO.jkyK.wqHhdy3z6dBR9JY2QOjd5Xrn/K', '522487967R'),
  ('Richard', 'Hernandez', '$2a$10$I9Xlsj8CUS9v6cZ2e9FkO.jkyK.wqHhdy3z6dBR9JY2QOjd5Xrn/K', '1234356D'),
  ('Pepe', 'Viyuela', '$2a$10$I9Xlsj8CUS9v6cZ2e9FkO.jkyK.wqHhdy3z6dBR9JY2QOjd5Xrn/K', '34567890K');

INSERT INTO ROLE (user_role_id, username, name) VALUES 
  (999, 'Samuel', 'ROLE_USER'),
  (998, 'Richard', 'ROLE_ADMIN');

INSERT INTO PRODUCT (code, description, price, state, CREATION_DATE, USER_ID) VALUES
  (999, 'Prueba1', 15, true, '1995-12-02', 'Samuel'),
  (998, 'Prueba2', 45, true, '2019-12-01', 'Richard'),
  (997, 'Prueba3', 5, false, '1985-12-10', 'Richard');
  
INSERT INTO SUPPLIER (id, name, country) VALUES
  (999, 'Transporter1', 'Spain'),
  (998, 'Transporter2', 'Canada'),
  (997, 'Transporter3', 'Country');
  
INSERT INTO PRICE_REDUCTION (id, REDUCED_PRICE, START_DATE, END_DATE) VALUES
  (999, 12.5, '2005-12-02', '2005-03-21'),
  (998, 10, '1995-12-05', '1996-02-01'),
  (997, 25.3, '2015-12-04', '2015-10-10');
  
INSERT INTO PRODUCT_SUPPLIER (product_code, supplier_id) VALUES
  (999, 998),
  (998, 997),
  (997, 999);
  
INSERT INTO PRODUCT_PRICE_REDUCTION(PRODUCT_CODE, PRICE_REDUCTION) VALUES
  (999, 999),
  (998, 998),
  (997, 997);


