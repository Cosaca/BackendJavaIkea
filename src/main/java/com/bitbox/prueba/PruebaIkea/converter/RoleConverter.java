package com.bitbox.prueba.PruebaIkea.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.RoleEntity;
import com.bitbox.prueba.PruebaIkea.model.RoleModel;

public class RoleConverter {

	public static RoleModel convertRoleEntityToRoleModel(RoleEntity entity)  {
		RoleModel rModel = new RoleModel();
		rModel.setId(entity.getId());
		rModel.setName(entity.getName());
		return rModel;
	}
	
	public static List<RoleModel> convertRoleEntityToRoleModel(List<RoleEntity> entities) {
		List<RoleModel> rModel = new ArrayList<RoleModel>();
		
		entities.forEach(entity -> {
			rModel.add(convertRoleEntityToRoleModel(entity));
		});
		
		return rModel;
	}
}
