package com.bitbox.prueba.PruebaIkea.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;
import com.bitbox.prueba.PruebaIkea.model.SupplierModel;

public class SupplierConverter {

	public static SupplierModel convertSupplierEntityToSuplierModel(SupplierEntity entity) {
		SupplierModel model = new SupplierModel();
		model.setCountry(entity.getCountry());
		model.setId(entity.getId());
		model.setName(entity.getName());
		
		return model;
	}
	
	public static List<SupplierModel> convertSupplierEntityToSuplierModel(List<SupplierEntity> entities) {
		List<SupplierModel> models = new ArrayList<SupplierModel>();
		entities.forEach(entity -> {
			models.add(convertSupplierEntityToSuplierModel(entity));
		});
		return models;
	}
}
