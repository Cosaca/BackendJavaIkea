package com.bitbox.prueba.PruebaIkea.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.UserEntity;
import com.bitbox.prueba.PruebaIkea.model.UserModel;

public class UserConverter {

	public static UserModel convertUserEntityToUserModel(UserEntity entity) {
		UserModel model = new UserModel();
		model.setUserName(entity.getUserName());
		model.setSurname(entity.getSurname());
		model.setDni(entity.getDni());
		model.setProducts(ProductConverter.convertProductEntityToProductModel(entity.getProducts()));
		model.setPassword(entity.getPassword());
		return model;
	}
	
	public static List<UserModel> convertUserEntityToUserModel(List<UserEntity> entities) {
		List<UserModel> userModel = new ArrayList<UserModel>();
		
		entities.forEach(entity -> {
			userModel.add(convertUserEntityToUserModel(entity));
		});
		
		return userModel;
	}
}
