package com.bitbox.prueba.PruebaIkea.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;
import com.bitbox.prueba.PruebaIkea.model.PriceReductionModel;

public class PriceReductionConverter {

	public static PriceReductionModel convertPriceReductionEntityToPriceReductionModel(PriceReductionEntity entity) {
		PriceReductionModel model = new PriceReductionModel();
		model.setId(entity.getId());
		model.setEndDate(entity.getEndDate());
		model.setReducedPrice(entity.getReducedPrice());
		model.setStartDate(entity.getStartDate());
		return model;
	}
	

	public static List<PriceReductionModel> convertPriceReductionEntityToPriceReductionModel(List<PriceReductionEntity> entities) {
		List<PriceReductionModel> model = new ArrayList<PriceReductionModel>();
		entities.forEach(entity -> {
			model.add(convertPriceReductionEntityToPriceReductionModel(entity));
		});
		return model;
	}
	
	
}
