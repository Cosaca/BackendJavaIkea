package com.bitbox.prueba.PruebaIkea.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.ProductEntity;
import com.bitbox.prueba.PruebaIkea.model.ProductModel;

public class ProductConverter {

	public static ProductModel convertProductEntityToProductModel(ProductEntity entity)  {
		ProductModel pModel = new ProductModel();
		pModel.setCode(entity.getCode());
		pModel.setCreationDate(entity.getCreationDate());
		pModel.setDescription(entity.getDescription());
		pModel.setPrice(entity.getPrice());
		pModel.setState(entity.getState());
		pModel.setUserCreator(entity.getUserCreator().getUserName());
		pModel.setPriceReductions(entity.getPriceReductionList() != null ? PriceReductionConverter.convertPriceReductionEntityToPriceReductionModel(entity.getPriceReductionList()) : null);
		pModel.setSupplierList(entity.getSupplierList() != null ? SupplierConverter.convertSupplierEntityToSuplierModel(entity.getSupplierList()) : null);
		return pModel;
	}
	
	public static List<ProductModel> convertProductEntityToProductModel(List<ProductEntity> entities)  {
		List<ProductModel> productos = new ArrayList<ProductModel>();
		entities.forEach(entity -> {
			productos.add(convertProductEntityToProductModel(entity));
		});
		
		return productos;
	}
	
}
