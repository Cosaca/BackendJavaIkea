package com.bitbox.prueba.PruebaIkea.request;

import java.util.Date;
import java.util.List;

import com.bitbox.prueba.PruebaIkea.model.PriceReductionModel;
import com.bitbox.prueba.PruebaIkea.model.SupplierModel;

public class ProductRequest {

	private Long code;
	private String description;
	private Double price;
	private Boolean state;
	private Date creationDate;
	private String idUserCreator;
	private List<SupplierModel> supplierList;
	private List<PriceReductionModel> priceReductions;
	
	public List<SupplierModel> getSupplierList() {
		return supplierList;
	}

	public void setSupplierList(List<SupplierModel> supplierList) {
		this.supplierList = supplierList;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdUserCreator() {
		return idUserCreator;
	}

	public void setIdUserCreator(String idUserCreator) {
		this.idUserCreator = idUserCreator;
	}

	public List<PriceReductionModel> getPriceReductions() {
		return priceReductions;
	}

	public void setPriceReductionsId(List<PriceReductionModel> priceReductions) {
		this.priceReductions = priceReductions;
	}
	
	

}
