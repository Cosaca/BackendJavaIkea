package com.bitbox.prueba.PruebaIkea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bitbox.prueba.PruebaIkea.model.SupplierModel;
import com.bitbox.prueba.PruebaIkea.service.ISupplierService;

@Controller
@RequestMapping("/private/suppliers")
@CrossOrigin(origins = "http://localhost:4200")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SupplierController {

	@Autowired
	private ISupplierService service;
	
	@GetMapping("/getAll")
	public ResponseEntity<List<SupplierModel>> getProducts() {
		HttpHeaders headers = new HttpHeaders();
		
		List<SupplierModel> respuesta = service.getAllSuppliers();
        
        return ResponseEntity.accepted().headers(headers).body(respuesta);
	}
	
}
