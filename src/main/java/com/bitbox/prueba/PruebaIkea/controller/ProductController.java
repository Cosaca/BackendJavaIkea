package com.bitbox.prueba.PruebaIkea.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bitbox.prueba.PruebaIkea.converter.ProductConverter;
import com.bitbox.prueba.PruebaIkea.model.ProductModel;
import com.bitbox.prueba.PruebaIkea.request.ProductRequest;
import com.bitbox.prueba.PruebaIkea.service.IProductService;

@Controller
@RequestMapping("/private/products")
@CrossOrigin(origins = "http://localhost:4200")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ProductController {

	@Autowired
	private IProductService productService;
	
	@GetMapping("/getAll")
	public ResponseEntity<List<ProductModel>> getProducts() {
		HttpHeaders headers = new HttpHeaders();
		
		List<ProductModel> respuesta = productService.getAllProducts();
        
        return ResponseEntity.accepted().headers(headers).body(respuesta);
	}
	
	@PostMapping("/addProduct")
	public ResponseEntity<ProductModel> addProduct(@RequestBody ProductRequest product) {
		try {
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Responded", "ProductController");
	        
			ProductModel respuesta = productService.createProduct(product);        
	        return ResponseEntity.accepted().headers(headers).body(respuesta);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
	
	@PostMapping("/updateProduct")
	public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductRequest product) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "ProductController");
        
        ProductModel respuesta = productService.updateProduct(product);
        if(respuesta == null)
        {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null); 
        }
        else {
        	return ResponseEntity.accepted().headers(headers).body(respuesta); 
        }
	}
	
	@GetMapping("/getProductById")
	public ResponseEntity<ProductModel> getProductById(@RequestParam Long id) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "ProductController");
        ProductModel respuesta = ProductConverter.convertProductEntityToProductModel(productService.findByCode(id));
        if(respuesta == null) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null); 
        } else {
        	return ResponseEntity.accepted().headers(headers).body(respuesta); 
        }   
	}
	
}
