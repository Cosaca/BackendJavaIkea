package com.bitbox.prueba.PruebaIkea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bitbox.prueba.PruebaIkea.model.UserModel;
import com.bitbox.prueba.PruebaIkea.service.IUserService;

@Controller
@RequestMapping("/private/users")
@CrossOrigin(origins = "http://localhost:4200")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class UserController {

	@Autowired
	private IUserService userService;
	
	@GetMapping("getUsers")
	public ResponseEntity<List<UserModel>> getAllUsers() {
		HttpHeaders headers = new HttpHeaders();
		
		List<UserModel> respuesta = userService.getAllUsers();
        
        return ResponseEntity.accepted().headers(headers).body(respuesta);
	}
	
	@GetMapping("getUsersById")
	public ResponseEntity<List<UserModel>> getUsersById(@RequestParam List<String> id) {
		HttpHeaders headers = new HttpHeaders();
		
		List<UserModel> respuesta = userService.findUsersById(id);
        
        return ResponseEntity.accepted().headers(headers).body(respuesta);
	}

    @GetMapping(value = "currentUser")
    public ResponseEntity<Object> getCurrentUser(){
        final Authentication auth= SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
        	HttpHeaders headers = new HttpHeaders();
        	 return ResponseEntity.accepted().headers(headers).body(auth.getPrincipal());
        }else {
        	 return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }

    }
}
