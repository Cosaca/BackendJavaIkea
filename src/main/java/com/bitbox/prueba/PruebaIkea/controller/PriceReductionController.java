package com.bitbox.prueba.PruebaIkea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bitbox.prueba.PruebaIkea.model.PriceReductionModel;
import com.bitbox.prueba.PruebaIkea.request.PriceReductionRequest;
import com.bitbox.prueba.PruebaIkea.service.IPriceReductionService;

@Controller
@RequestMapping("/private/priceReduction")
@CrossOrigin(origins = "http://localhost:4200")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class PriceReductionController {

	@Autowired
	private IPriceReductionService priceReductionService;
	
	@GetMapping("/getPriceReductions")
	public ResponseEntity<List<PriceReductionModel>> getPriceReductions() {
		HttpHeaders headers = new HttpHeaders();
		
		List<PriceReductionModel> respuesta = priceReductionService.getAllPriceReductions();
        
        return ResponseEntity.accepted().headers(headers).body(respuesta);
	}
	
	@GetMapping("/addPriceReduction")
	public ResponseEntity<PriceReductionModel> addPriceReduction(@RequestBody PriceReductionRequest priceRed) {
		try {
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Responded", "PriceReductionController");
	        
			PriceReductionModel respuesta = priceReductionService.createPriceRed(priceRed);        
	        return ResponseEntity.accepted().headers(headers).body(respuesta);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
}
