package com.bitbox.prueba.PruebaIkea.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;

@Repository("priceReductionRepository")
public interface PriceReductionRepository extends JpaRepository<PriceReductionEntity, Serializable>{
	
}
