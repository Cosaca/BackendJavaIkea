package com.bitbox.prueba.PruebaIkea.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bitbox.prueba.PruebaIkea.entity.UserEntity;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<UserEntity, Serializable>{

	UserEntity findByUsername(String name);
	
}
