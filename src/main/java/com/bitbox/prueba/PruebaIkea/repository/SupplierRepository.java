package com.bitbox.prueba.PruebaIkea.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;

@Repository("supplierRepository")
public interface SupplierRepository extends JpaRepository<SupplierEntity, Serializable>{

}
