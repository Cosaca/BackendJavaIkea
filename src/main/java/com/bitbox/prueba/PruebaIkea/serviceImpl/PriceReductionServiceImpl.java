package com.bitbox.prueba.PruebaIkea.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.converter.PriceReductionConverter;
import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;
import com.bitbox.prueba.PruebaIkea.model.PriceReductionModel;
import com.bitbox.prueba.PruebaIkea.repository.PriceReductionRepository;
import com.bitbox.prueba.PruebaIkea.request.PriceReductionRequest;
import com.bitbox.prueba.PruebaIkea.service.IPriceReductionService;

@Service
public class PriceReductionServiceImpl implements IPriceReductionService {

	@Autowired
	@Qualifier("priceReductionRepository")
	private PriceReductionRepository repository;

	@Override
	public PriceReductionEntity findById(int id) {
		Optional<PriceReductionEntity> entity = repository.findById(id);

		if (entity.isPresent()) {
			return entity.get();
		}

		return null;
	}

	@Override
	public PriceReductionEntity add(Double reducedPrice, Date startDate, Date endDate) {
		PriceReductionEntity entity = new PriceReductionEntity();

		entity.setStartDate(startDate);
		entity.setEndDate(endDate);
		entity.setReducedPrice(reducedPrice);

		return repository.save(entity);
	}

	@Override
	public int deleteById(int id) {
		repository.deleteById(id);
		return id;
	}

	@Override
	public List<PriceReductionEntity> findAll() {
		return repository.findAll();
	}

	@Override
	public PriceReductionEntity update(int id, Double reducedPrice, Date startDate, Date endDate) {
		PriceReductionEntity entity = new PriceReductionEntity();

		entity.setId(id);
		entity.setStartDate(startDate);
		entity.setEndDate(endDate);
		entity.setReducedPrice(reducedPrice);

		return repository.save(entity);
	}

	@Override
	public PriceReductionEntity getPriceReductionById(int id) {
		return this.findById(id);
	}

	@Override
	public List<PriceReductionEntity> getPriceReductions(List<Integer> ids) {
		List<PriceReductionEntity> priceReductions = new ArrayList<PriceReductionEntity>();
		ids.forEach(id -> {
			priceReductions.add(getPriceReductionById(id));
		});

		return priceReductions;
	}

	@Override
	public List<PriceReductionModel> getAllPriceReductions() {
		return PriceReductionConverter.convertPriceReductionEntityToPriceReductionModel(findAll());
	}

	@Override
	public PriceReductionModel createPriceRed(PriceReductionRequest request) {
		PriceReductionEntity entity = this.add(request.getReducedPrice(), request.getStartDate(), request.getEndDate());
		return PriceReductionConverter.convertPriceReductionEntityToPriceReductionModel(entity);
	}

}
