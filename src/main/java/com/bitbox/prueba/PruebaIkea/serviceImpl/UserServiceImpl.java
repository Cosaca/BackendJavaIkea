package com.bitbox.prueba.PruebaIkea.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.converter.UserConverter;
import com.bitbox.prueba.PruebaIkea.entity.RoleEntity;
import com.bitbox.prueba.PruebaIkea.entity.UserEntity;
import com.bitbox.prueba.PruebaIkea.model.UserModel;
import com.bitbox.prueba.PruebaIkea.repository.UserRepository;
import com.bitbox.prueba.PruebaIkea.service.IUserService;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService{

	@Autowired
	@Qualifier("userRepository")
	private UserRepository repository;
	
	@Override
	public UserEntity findById(String id) {
		UserEntity entity = repository.findByUsername(id);

		return entity;
	}

	@Override
	public UserEntity add(String username, String surname, String dni, String password) {
		UserEntity entity = new UserEntity();
		
		entity.setUserName(username);
		entity.setSurname(surname);
		entity.setDni(dni);
		entity.setPassword(password);
		
		return repository.save(entity);
	}

	@Override
	public int deleteById(int id) {
		repository.deleteById(id);
		return id;
	}

	@Override
	public List<UserEntity> findAll() {
		return repository.findAll();
	}

	@Override
	public UserEntity update(String username, String surname, String dni, String password) {
		UserEntity entity = new UserEntity();
			
		entity.setUserName(username);
		entity.setSurname(surname);
		entity.setDni(dni);
		entity.setPassword(password);
		
		return repository.save(entity);
	}
	
	@Override 
	public List<UserModel> getAllUsers() {
		List<UserModel> users = UserConverter.convertUserEntityToUserModel(this.findAll());
		return users;
	}
	
	@Override
	public List<UserModel> findUsersById(List<String> ids) {
		List<UserModel> users = new ArrayList<UserModel>();
		
		ids.forEach(id -> {
			users.add(UserConverter.convertUserEntityToUserModel(repository.findByUsername(id)));
		});
		
		return users;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = repository.findByUsername(username);
		List<GrantedAuthority> authorities = buildAuthorities(user.getRoles());
		return buildUser(user, authorities);
	}

	private UserDetails buildUser(UserEntity user, List<GrantedAuthority> authorities) {
		return new User(user.getUserName(), user.getPassword(), true, true, true, true, authorities);
	}

	private List<GrantedAuthority> buildAuthorities(List<RoleEntity> roles) {
		List<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
		
		for (RoleEntity role : roles) {
			auths.add(new SimpleGrantedAuthority(role.getName()));
		}
		
		return new ArrayList<GrantedAuthority>(auths);
	}
}
