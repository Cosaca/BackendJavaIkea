package com.bitbox.prueba.PruebaIkea.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.converter.SupplierConverter;
import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;
import com.bitbox.prueba.PruebaIkea.model.SupplierModel;
import com.bitbox.prueba.PruebaIkea.repository.SupplierRepository;
import com.bitbox.prueba.PruebaIkea.service.ISupplierService;

@Service
public class SupplierServiceImpl implements ISupplierService{

	@Autowired
	@Qualifier("supplierRepository")
	private SupplierRepository repository;
	
	@Override
	public SupplierEntity findById(int id) {
		Optional<SupplierEntity> entity = repository.findById(id);

		if(entity.isPresent()) {
			return entity.get();
		}
			
		return null;
	}

	@Override
	public int deleteById(int id) {
		repository.deleteById(id);
		return id;
	}

	@Override
	public SupplierEntity add(String name, String country) {
		SupplierEntity entity = new SupplierEntity();
		
		entity.setName(name);
		entity.setCountry(country);
		
		return repository.save(entity);
	}

	@Override
	public List<SupplierEntity> findAll() {
		return repository.findAll();
	}

	@Override
	public SupplierEntity update(int id, String name, String country) {
		SupplierEntity entity = new SupplierEntity();
		
		entity.setId(id);
		entity.setName(name);
		entity.setCountry(country);
		
		return repository.save(entity);
	}
	
	@Override
	public SupplierEntity findSupplier(int id) {
		return findById(id);
	}
	
	@Override
	public List<SupplierEntity> findSuppliers(List<Integer> ids) {
		List<SupplierEntity> suppliers = new ArrayList<SupplierEntity>();
		
		ids.forEach(i -> {
			suppliers.add(findById(i));
		});
		
		return suppliers;
	}

	@Override
	public List<SupplierModel> getAllSuppliers() {
		return SupplierConverter.convertSupplierEntityToSuplierModel(this.findAll());
	}

}
