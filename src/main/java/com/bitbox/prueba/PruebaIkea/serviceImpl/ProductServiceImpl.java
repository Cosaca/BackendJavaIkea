package com.bitbox.prueba.PruebaIkea.serviceImpl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.converter.ProductConverter;
import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;
import com.bitbox.prueba.PruebaIkea.entity.ProductEntity;
import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;
import com.bitbox.prueba.PruebaIkea.entity.UserEntity;
import com.bitbox.prueba.PruebaIkea.model.ProductModel;
import com.bitbox.prueba.PruebaIkea.repository.ProductRepository;
import com.bitbox.prueba.PruebaIkea.request.ProductRequest;
import com.bitbox.prueba.PruebaIkea.service.IPriceReductionService;
import com.bitbox.prueba.PruebaIkea.service.IProductService;
import com.bitbox.prueba.PruebaIkea.service.ISupplierService;
import com.bitbox.prueba.PruebaIkea.service.IUserService;

@Service
public class ProductServiceImpl implements IProductService{

	@Autowired
	@Qualifier("productRepository")
	private ProductRepository repository;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ISupplierService supplierService;
	
	@Autowired
	private IPriceReductionService priceReductionService;
	
	@Override
	public ProductEntity findByCode(Long code) {
		Optional<ProductEntity> entity = repository.findById(code);

		if(entity.isPresent()) {
			return entity.get();
		}
			
		return null;
	}

	@Override
	@Transactional
	public ProductEntity add(String description, Double price, Boolean state, Date creationDate, UserEntity userCreator, 
			List<SupplierEntity> suppliers, List<PriceReductionEntity> priceReductions) {
		
		ProductEntity entity = new ProductEntity();

		entity.setDescription(description);
		entity.setPrice(price != null ? price : 0.0);
		entity.setState(state);
		entity.setUserCreator(userCreator);
		entity.setCreationDate(Date.from(Instant.now()));
		entity.setSupplierList(suppliers);
		entity.setPriceReductionList(priceReductions);
		
		return repository.save(entity);
	}

	@Override
	public int deleteByCode(Long code) {
		repository.deleteById(code);
		return code.intValue();
	}

	@Override
	public List<ProductEntity> findAll() {
		return repository.findAll();
	}

	@Override
	public ProductEntity update(Long code, String description, Double price, Boolean state, 
			Date creationDate, UserEntity userCreator, List<SupplierEntity> suppliers, List<PriceReductionEntity> priceReductions) {
		ProductEntity entity = new ProductEntity();
	
		entity.setCode(code);
		entity.setDescription(description);
		entity.setPrice(price);
		entity.setState(state);
		entity.setUserCreator(userCreator);
		entity.setCreationDate(creationDate);
		
		if(suppliers != null && suppliers.size() > 0) {
			entity.setSupplierList(suppliers);
		}
		
		if(priceReductions != null && priceReductions.size() > 0) {
			entity.setPriceReductionList(priceReductions);
		}
		
		return repository.save(entity);
	}
	
	@Override
	public ProductModel createProduct(ProductRequest request) {
		UserEntity usuario = null;
		if(request.getIdUserCreator() != null) {
			usuario = userService.findById(request.getIdUserCreator());
		}
		
		List<SupplierEntity> suppliersList = new ArrayList<SupplierEntity>();
		List<PriceReductionEntity> priceReductions = new ArrayList<PriceReductionEntity>();
		
		checkSuppliers(request, suppliersList);
		
		checkPriceReductions(request, priceReductions);
		
		ProductEntity pEntity = this.add(request.getDescription(), request.getPrice(), 
				request.getState(), request.getCreationDate(), usuario, suppliersList, priceReductions);
		
		return ProductConverter.convertProductEntityToProductModel(pEntity);
	}

	private void checkPriceReductions(ProductRequest request, List<PriceReductionEntity> priceReductions) {
		if(request.getPriceReductions() != null) {
			request.getPriceReductions().forEach(priceReduction -> {
				if(priceReduction.getId() == null) {
					PriceReductionEntity entity = priceReductionService.add(priceReduction.getReducedPrice(), priceReduction.getStartDate(), priceReduction.getEndDate());
					priceReductions.add(entity);
				}else {
					priceReductions.add(priceReductionService.findById(priceReduction.getId()));
				}
			});
		}
	}

	private void checkSuppliers(ProductRequest request, List<SupplierEntity> suppliersList) {
		//Comprobar si se le pasa algun transportista
		if(request.getSupplierList() != null) {
			request.getSupplierList().forEach(supplier -> {
				suppliersList.add(supplierService.findById(supplier.getId().intValue()));
			});
		}
	}
	
	@Override
	public List<ProductModel> getAllProducts() {
		return ProductConverter.convertProductEntityToProductModel(findAll());
	}
	
	@Override
	public ProductModel updateProduct(ProductRequest request) {
		ProductEntity pEntityBD = this.findByCode(request.getCode());
		List<SupplierEntity> suppliersList = new ArrayList<SupplierEntity>();
		List<PriceReductionEntity> priceReductions = new ArrayList<PriceReductionEntity>();
		
		//Si no esta activo
		if(!pEntityBD.getState())
			return null;
		
		checkSuppliers(request, suppliersList);
		
		UserEntity usuario = userService.findById(request.getIdUserCreator());
		
		checkPriceReductions(request, priceReductions);
		
		ProductEntity pEntity = this.update(request.getCode(), request.getDescription(), request.getPrice(), 
				request.getState(), request.getCreationDate(), usuario, suppliersList, priceReductions);
		
		return ProductConverter.convertProductEntityToProductModel(pEntity);
	}
}
