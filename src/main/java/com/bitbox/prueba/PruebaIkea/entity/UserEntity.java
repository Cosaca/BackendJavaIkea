package com.bitbox.prueba.PruebaIkea.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="USER")
public class UserEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8683183059249081842L;
	
	@Id
	@Column(name="username", unique=true, nullable=false)
	private String username;
	
	@Column
	private String surname;
	
	@Column(name="password", nullable=false)
	private String password;
	
	@Column
	private String dni;
	
	@OneToMany(mappedBy = "userCreator")
	private List<ProductEntity> products;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy = "user")
	private List<RoleEntity> roles;

	public UserEntity() {}

	public UserEntity(String username, String surname, String password, String dni, List<RoleEntity> roles) {
		super();
		this.username = username;
		this.surname = surname;
		this.password = password;
		this.dni = dni;
		this.roles = roles;
	}

	public List<ProductEntity> getProducts() {
		return products;
	}

	public void setProducts(List<ProductEntity> products) {
		this.products = products;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public List<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleEntity> roles) {
		this.roles = roles;
	}
	
}
