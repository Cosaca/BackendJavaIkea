package com.bitbox.prueba.PruebaIkea.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity(name = "PRODUCT")
public class ProductEntity implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = 3343407501306954846L;

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long code;

	@Column(nullable = false)
	private String description;

	@Column
	private Double price;

	@Column(name = "STATE", columnDefinition = "boolean default true", nullable = false)
	private Boolean state = true;

	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	private UserEntity userCreator;

	@Column
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "PRODUCT_SUPPLIER", joinColumns = @JoinColumn(name = "PRODUCT_CODE"), inverseJoinColumns = @JoinColumn(name = "SUPPLIER_ID"))
	private List<SupplierEntity> supplierList;

	@Column
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "PRODUCT_PRICE_REDUCTION", joinColumns = @JoinColumn(name = "PRODUCT_CODE"), inverseJoinColumns = @JoinColumn(name = "PRICE_REDUCTION"))
	private List<PriceReductionEntity> priceReductionList;

	public ProductEntity() {
	}

	public ProductEntity(Long code, String description, Double price, Boolean state, Date creationDate,
			UserEntity userCreator, List<SupplierEntity> supplierList, List<PriceReductionEntity> priceReductionList) {
		super();
		this.code = code;
		this.description = description;
		this.price = price;
		this.state = state;
		this.creationDate = creationDate;
		this.userCreator = userCreator;
		this.supplierList = supplierList;
		this.priceReductionList = priceReductionList;
	}

	public UserEntity getUserCreator() {
		return userCreator;
	}
	
	public List<PriceReductionEntity> getPriceReductionList() {
		return priceReductionList;
	}

	public void setPriceReductionList(List<PriceReductionEntity> priceReductionList) {
		this.priceReductionList = priceReductionList;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public void setUserCreator(UserEntity userCreator) {
		this.userCreator = userCreator;
	}

	public List<SupplierEntity> getSupplierList() {
		return supplierList;
	}

	public void setSupplierList(List<SupplierEntity> supplierList) {
		this.supplierList = supplierList;
	}

}
