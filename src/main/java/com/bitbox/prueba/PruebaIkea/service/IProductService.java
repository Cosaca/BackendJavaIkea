package com.bitbox.prueba.PruebaIkea.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;
import com.bitbox.prueba.PruebaIkea.entity.ProductEntity;
import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;
import com.bitbox.prueba.PruebaIkea.entity.UserEntity;
import com.bitbox.prueba.PruebaIkea.model.ProductModel;
import com.bitbox.prueba.PruebaIkea.request.ProductRequest;

@Service
public interface IProductService {

	public ProductEntity findByCode(Long code);
	
	public int deleteByCode(Long code);

	public List<ProductEntity> findAll();
	
	public ProductEntity update(Long code, String description, Double price, Boolean state, Date creationDate, UserEntity userCreator, List<SupplierEntity> suppliers, List<PriceReductionEntity> reductions);

	ProductModel createProduct(ProductRequest request);

	List<ProductModel> getAllProducts();

	ProductModel updateProduct(ProductRequest request);

	ProductEntity add(String description, Double price, Boolean state, Date creationDate, UserEntity userCreator,
			List<SupplierEntity> suppliers, List<PriceReductionEntity> priceReductions);
}
