package com.bitbox.prueba.PruebaIkea.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.entity.PriceReductionEntity;
import com.bitbox.prueba.PruebaIkea.model.PriceReductionModel;
import com.bitbox.prueba.PruebaIkea.request.PriceReductionRequest;

@Service
public interface IPriceReductionService {

	public PriceReductionEntity findById(int id);
	
	public PriceReductionEntity add(Double reducedPrice, Date startDate, Date endDate);
	
	public int deleteById(int id);
	
	public List<PriceReductionEntity> findAll();
	
	public PriceReductionEntity update(int id, Double reducedPrice, Date startDate, Date endDate);

	PriceReductionEntity getPriceReductionById(int id);

	List<PriceReductionEntity> getPriceReductions(List<Integer> ids);

	List<PriceReductionModel> getAllPriceReductions();

	PriceReductionModel createPriceRed(PriceReductionRequest request);
	
}
