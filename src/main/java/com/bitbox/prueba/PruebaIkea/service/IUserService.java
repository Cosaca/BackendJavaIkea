package com.bitbox.prueba.PruebaIkea.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.entity.UserEntity;
import com.bitbox.prueba.PruebaIkea.model.UserModel;

@Service
public interface IUserService {
	
	public UserEntity add(String name, String surname, String dni, String password);
	
	public int deleteById(int id);

	public List<UserEntity> findAll();
	
	public UserEntity update(String username, String surname, String dni, String password);

	List<UserModel> getAllUsers();


	List<UserModel> findUsersById(List<String> ids);

	UserEntity findById(String id);
	
}
