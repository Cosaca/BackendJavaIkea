package com.bitbox.prueba.PruebaIkea.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bitbox.prueba.PruebaIkea.entity.SupplierEntity;
import com.bitbox.prueba.PruebaIkea.model.SupplierModel;

@Service
public interface ISupplierService {

	public SupplierEntity findById(int id);

	public int deleteById(int id);
	
	public SupplierEntity add(String name, String country);
	
	public List<SupplierEntity> findAll();
	
	public SupplierEntity update(int id, String name, String country);

	SupplierEntity findSupplier(int id);

	List<SupplierEntity> findSuppliers(List<Integer> ids);

	public List<SupplierModel> getAllSuppliers();
	
}
