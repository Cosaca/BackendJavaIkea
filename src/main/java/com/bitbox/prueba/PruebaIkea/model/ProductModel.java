package com.bitbox.prueba.PruebaIkea.model;

import java.util.Date;
import java.util.List;

public class ProductModel {

	private Long code;

	private String description;

	private Double price;

	private Boolean state;

	private Date creationDate;

	private String userCreator;

	private List<SupplierModel> supplierList;
	
	private List<PriceReductionModel> priceReductions;

	public ProductModel() {}
	
	public ProductModel(Long code, String description, Double price, Boolean state, Date creationDate,
			String userCreator, List<SupplierModel> supplierList) {
		super();
		this.code = code;
		this.description = description;
		this.price = price;
		this.state = state;
		this.creationDate = creationDate;
		this.userCreator = userCreator;
		this.supplierList = supplierList;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserCreator() {
		return userCreator;
	}

	public void setUserCreator(String userCreator) {
		this.userCreator = userCreator;
	}

	public List<SupplierModel> getSupplierList() {
		return supplierList;
	}

	public void setSupplierList(List<SupplierModel> supplierList) {
		this.supplierList = supplierList;
	}

	public List<PriceReductionModel> getPriceReductions() {
		return priceReductions;
	}

	public void setPriceReductions(List<PriceReductionModel> priceReductions) {
		this.priceReductions = priceReductions;
	}
	
	
}
