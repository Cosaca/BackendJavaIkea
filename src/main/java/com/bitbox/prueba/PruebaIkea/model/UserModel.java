package com.bitbox.prueba.PruebaIkea.model;

import java.util.List;

import com.bitbox.prueba.PruebaIkea.entity.RoleEntity;

public class UserModel {
	
	private String username;
	
	private String surname;
	
	private String password;

	private String dni;

	private List<ProductModel> products;
	
	private List<RoleEntity> roles;

	public UserModel() {}
	
	public UserModel(String username, String surname, String password, String dni,
			List<ProductModel> products, List<RoleEntity> roles) {
		super();
		this.username = username;
		this.surname = surname;
		this.password = password;
		this.dni = dni;
		this.products = products;
		this.roles = roles;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public List<ProductModel> getProducts() {
		return products;
	}

	public void setProducts(List<ProductModel> products) {
		this.products = products;
	}

	public List<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleEntity> roles) {
		this.roles = roles;
	}
	
}
