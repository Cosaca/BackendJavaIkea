package com.bitbox.prueba.PruebaIkea.model;

import java.util.Date;

public class PriceReductionModel {

	private Integer id;
	
	private Double reducedPrice;

	private Date startDate;

	private Date endDate;

	public PriceReductionModel() {}
	
	public PriceReductionModel(Integer id, Double reducedPrice, Date startDate, Date endDate) {
		super();
		this.id = id;
		this.reducedPrice = reducedPrice;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getReducedPrice() {
		return reducedPrice;
	}

	public void setReducedPrice(Double reducedPrice) {
		this.reducedPrice = reducedPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
